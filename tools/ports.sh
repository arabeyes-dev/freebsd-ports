#!/bin/sh
#
# Copyright (c) 2003-2005, Samy Al Bahra <samy@kerneled.org>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice unmodified, this list of conditions, and the following
#    disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

PATH=/bin:/usr/bin:/sbin:/usr/sbin
PORTLINT="./portlint -A"

if [ $# -lt 2 ]; then
	echo "Usage: $0 <ports directory> <log directory>"
fi

if [ ! -d "$1" ] || [ ! -r "$1" ]; then
	echo "$1 is not a readable directory"
	exit 1
fi

PORTS=`realpath $1`
LOG="$2"

if [ -f "$LOG" ]; then
	mv $LOG `echo $LOG|sed s/.html/.recent.html/`
fi

echo "<HTML><HEAD><TITLE>FreeBSD Arabic ports log</TITLE></HEAD>" \
     "<BODY><FONT face="Verdana, Helvetica, Arial" size=\"2\">" >> $LOG

for i in `find $PORTS/* -type d`; do

	if [ ! -r "$i/Makefile" ]; then
		continue;
	fi

	echo "<B><FONT color=\"#0000FF\"> Checking..." \
	     `cat $i/Makefile|grep PORTNAME=|sed 's|PORTNAME=||'` \
	     "</B></FONT><BR><BR>" >> $LOG

	$PORTLINT $i | \
	awk   '{
			if ( $1 == "FATAL:")
				print "<FONT color=\"#FF0000\"><B>"$0"</B></FONT><BR>";
			else if ( $1 == "WARN:" )
				printf "<B>"$0"</B><BR>\n";
			else
				printf "<FONT color=\"#777777\">"$0"</FONT><BR>\n"
		}' >> $LOG

	echo "<HR>" >> $LOG
done

echo "</FONT></BODY></HTML>" >> $LOG
